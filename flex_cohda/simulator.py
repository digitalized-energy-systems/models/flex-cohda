from eocohda.core.simulator import ConstraintCheckingScheduleSimulator


class ConstraintCheckingScheduleFlexSimulator(
    ConstraintCheckingScheduleSimulator):

    def _on_simulate(self, slot, energy, i):
        """
        Override
        """
        if not self._valid:
            return

        abs_energy = abs(energy)
        self._valid = \
            self._valid \
            and self._energy_storage.min_allowed_soc_at(i) <= \
                self._energy_storage.load <= \
                self._energy_storage.max_allowed_soc_at(i) \
            and ((not slot.strategy.is_charge)
                 or abs_energy <=
                 self._energy_storage.max_charge_at(i) +
                 ConstraintCheckingScheduleSimulator.COMPARE_TOLERANCE) \
            and (slot.strategy.is_charge
                 or abs_energy <=
                 self._energy_storage.max_discharge_at(i) +
                 ConstraintCheckingScheduleSimulator.COMPARE_TOLERANCE)

        if not self._valid:
            pass

        self._valid = \
            self._valid \
            and abs_energy >= 0 \
            and ((not slot.strategy.is_charge) or energy >= 0) \
            and (slot.strategy.is_charge or energy <= 0) \
            and slot.strategy not in self._blacklist

        if not self._valid:
            pass

        self._counter += 1
