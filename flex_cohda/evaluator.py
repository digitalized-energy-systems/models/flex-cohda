import numpy as np

from interface import implements
from overrides import overrides

from eocohda.core.evaluator import EvaluatorModel, EvaluatorPlugin
from flex_cohda.simulator import ConstraintCheckingScheduleFlexSimulator


class FlexEvaluator(EvaluatorModel):
    """
        Wrapper for an domain evaluator plugins. Calculates the penalty for
        violating constraints.
    """

    NOT_VALID = -9999999999
    THRESHOLD = 0

    @overrides
    def _perform_constraint_check(self, solution):
        """
        Performs a constraint check.

        :returns: true if valid, false otherwise
        """
        input_list = np.zeros(
            solution.size()) if self._input_list is None else self._input_list
        simulator = \
            ConstraintCheckingScheduleFlexSimulator(self._storage,
                                                    input_list,
                                                    self._blacklist)
        return simulator.simulate(solution.slots)


class FlexCohdaEvaluatorPlugin(implements(EvaluatorPlugin)):

    def __init__(self, storage, cohda_objective):
        self.__storage = storage
        self.__cohda_objective = cohda_objective

    @overrides
    def eval_step(self, solution, prior_rating):
        schedule = self.__storage.convert_candidate_to_schedule(solution)
        return prior_rating + self.__cohda_objective(schedule)
