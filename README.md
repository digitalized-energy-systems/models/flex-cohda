# Flex COHDA

## Description
Flex COHDA contains several algorithms for generating multi criteria optimized schedules for integrating flexibility with wide sets of objectives in a COHDA negotiation.

## Installation
TODO

## Usage
TODO


## Authors and acknowledgment
The des-flex to COHDA adapter was developed by Stephan Ferenz[^1] and Rico Schrage[^1].
_____

[^1]: [Department for Computer Science, Carl von Ossietzky University of Oldenburg](https://uol.de/des)

## License
The code is distributed under the MIT license.
