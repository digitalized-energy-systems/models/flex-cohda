import pickle
from des_flex.flex_class import Flexibility
from flex_cohda.schedule_generator import ScheduleGenerator
from flex_cohda.visualization import visualize_flex_schedules


def test_flexibility_to_schedule():
    loaded_flex = pickle.load(open("tests/flexibility.p", "rb"))
    flex = Flexibility(
        flex_max_power=loaded_flex['flex_max_power'],
        flex_min_power=loaded_flex['flex_min_power'],
        flex_max_energy_delta=loaded_flex['flex_max_energy_delta'],
        flex_min_energy_delta=loaded_flex['flex_min_energy_delta']
    )

    schedule_generator = ScheduleGenerator(flexibility=flex)
    schedules = schedule_generator.create_schedules()

    schedule_generator.update_flexibility(flex)
    schedules = schedule_generator.create_schedules()

    assert schedules is not None

    visualize_flex_schedules(flex, schedules, 15 * 60)


if __name__ == '__main__':
    test_flexibility_to_schedule()
