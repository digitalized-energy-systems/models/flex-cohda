import pickle
from des_flex.flex_class import Flexibility
from flex_cohda.flexstorage import FlexStorage
from flex_cohda.schedule_generator import ScheduleGenerator
from flex_cohda.visualization import visualize_flex_schedules
import numpy as np


def test_storage():
    loaded_flex = pickle.load(open("tests/flexibility.p", "rb"))
    flex = Flexibility(
        flex_max_power=loaded_flex['flex_max_power'],
        flex_min_power=loaded_flex['flex_min_power'],
        flex_max_soc=np.zeros(len(loaded_flex['flex_max_power'])),
        flex_min_soc=np.zeros(len(loaded_flex['flex_max_power'])),
        max_plan_soc=np.zeros(len(loaded_flex['flex_max_power'])),
        min_plan_soc=np.zeros(len(loaded_flex['flex_max_power'])),
        flex_max_energy_delta=loaded_flex['flex_max_energy_delta'],
        flex_min_energy_delta=loaded_flex['flex_min_energy_delta']
    )
    time_resolution = 15 * 60
    storage = FlexStorage(flex, time_resolution)
    print(flex.flex_max_power[0], flex.flex_min_power[0],
          flex.flex_max_energy_delta[0], flex.flex_min_energy_delta[0])

    max_charge_energy = min(storage.max_charge_at(0),
                            storage.load_until_cap_at(0))
    max_discharge_energy = min(storage.max_discharge_at(0),
                               storage.remaining_load_at(0))
    print(max_charge_energy)
    print(max_discharge_energy)

    soc_before_max_charge = storage.load
    soc_after_max_charge = storage.charge(max_charge_energy)

    print(soc_before_max_charge, soc_after_max_charge, storage.load)

    power = (soc_after_max_charge-soc_before_max_charge) * \
            storage.load_to_energy_factor() * 3600/time_resolution

    print(power)




if __name__ == '__main__':
    test_storage()
