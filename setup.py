from setuptools import setup, find_packages

# keep this file in order to be able to run "pip install -e ." for better
# use of this project inside others
setup(name='flex-COHDA',
      version='0.0.1',
      description='An adapter to create multiple valid schedules from '
                  'des-flex flexibility',
      url='https://gitlab.com/digitalized-energy-systems/models/flex-cohda',
      author='Stephan Ferenz, Rico Schrage',
      packages=find_packages(where='.'))
